public class Cash {
    double value;
    PaymentMethod paymentMethod;

    public Cash(double value){
        this.paymentMethod = PaymentMethod.CASH;
        this.value = value;
    }

    public double balance(){
        return value;
    }

    public void pay(double spend){
        if(this.value >= spend){
            System.out.println("Successful payment!");
            this.value-=spend;
        }else{
            System.out.println("Not enough cash to pay for it.");
        }
    }

    public PaymentMethod getPaymentMethod(){
        return this.paymentMethod;
    }
}
