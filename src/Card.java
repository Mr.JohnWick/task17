public abstract class Card {
    double balance;
    String cardNr;
    PaymentMethod cardType;

    public Card(double balance, String cardNr, PaymentMethod cardType){
        this.balance = balance;
        this.cardNr = cardNr;
        this.cardType = cardType;
    }

    public void pay(double spend){
            if(this.balance >= spend){
                System.out.println("Successful payment!");
                this.balance-=spend;
            }else{
                System.out.println("insufficient funds.");
            }
    }
    public String getCardBalance(){
        return ("Your current balance "+this.balance);
    }

    public PaymentMethod getCardType(){
        return this.cardType;
    }

}
