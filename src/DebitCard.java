public class DebitCard extends Card{

    public DebitCard(double balance, String cardNr){
        super(balance,cardNr, PaymentMethod.DEBIT);
    }
}
