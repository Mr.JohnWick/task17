public class CreditCard extends Card{


    public CreditCard(double balance, String cardNr){
        super(balance, cardNr, PaymentMethod.CREDIT);
    }
}
