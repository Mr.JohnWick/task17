public class CardOwner {
    String firstName;
    String lastName;
    CreditCard credit;
    DebitCard debit;
    Cash cash;

    public CardOwner(String firstName, String lastName, CreditCard credit, DebitCard debit, Cash cash){
        this.firstName = firstName;
        this.lastName = lastName;
        this.credit = credit;
        this.debit = debit;
        this.cash = cash;
    }
}
