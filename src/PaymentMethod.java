public enum PaymentMethod {
    CREDIT(1),
    DEBIT(2),
    CASH(3);

    final int id;

    PaymentMethod(int id){
        this.id = id;
    }
}
