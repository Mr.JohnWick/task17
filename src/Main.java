import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        CreditCard credit = new CreditCard(1000,"1234");
        DebitCard debit = new DebitCard(1000, "456");
        Cash cash = new Cash(500);
        CardOwner john = new CardOwner("John","Wick",credit,debit, cash);
        int paymentMethod = 0;
        double payingFor =  250;

        try{
            System.out.println("Select payment method 1) Credit 2) Debit 3) Cash 4) Dine and Dash");
            paymentMethod = userInput.nextInt();
        }catch(InputMismatchException e){
            System.out.println("We need a numeric input "+ e);
        }
        switch (paymentMethod){
            case 1:
                System.out.println("Paying with "+john.credit.getCardType());
                System.out.println("The item you are paying for costs "+payingFor);
                john.credit.pay(payingFor);
                System.out.println(john.credit.getCardBalance());
            break;
            case 2:
                System.out.println("Paying with "+john.debit.getCardType());
                System.out.println("The item you are paying for costs "+payingFor);
                john.debit.pay(payingFor);
                System.out.println(john.debit.getCardBalance());
            break;
            case 3:
                System.out.println("Paying with "+john.cash.getPaymentMethod());
                System.out.println("The item you are paying for costs "+payingFor);
                john.cash.pay(payingFor);
                System.out.println(john.cash.balance());
            break;
            case 4:
                System.out.println("Run you fools!");
                break;
            default:
                System.out.println("Please select one of the provided options");
        }
    }
}
